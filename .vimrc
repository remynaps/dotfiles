" General Confiration
" .....................................
syntax on
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
Plugin 'airblade/vim-gitgutter'
Plugin 'kien/ctrlp.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'bling/vim-airline'
Plugin 'Shougo/neocomplete.vim'
"Plugin 'Valloric/YouCompleteMe'

"colorschemes
Plugin 'w0ng/vim-hybrid'

" All of your Plugins must be added before the following line
call vundle#end()            
" required            	" this is needed to see syntax

map <C-n> :NERDTreeToggle<CR>

"colors
let g:hybrid_use_Xresources = 1
colorscheme hybrid

"behavior
set mouse=a
set clipboard=unnamedplus
set ttymouse=xterm2
set nu
set tabstop=4
"neocomplete
let g:neocomplete#enable_at_startup = 1

"airline
let g:airline_theme="hybridline"
set noshowmode
let g:airline#extensions#tabline#enabled = 1
set laststatus=2
let g:airline_powerline_fonts = 1
